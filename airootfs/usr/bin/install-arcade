#!/bin/bash
#
# ab-install v0.3
#
# Based on the Arch Installation Script (AIS) and Arch Ultimate Installation
# script (AUI) written by helmuthdu (helmuthdu[at]gmail[dot]com). Modified by
# Carl Duff for Evo/Lution Linux.
#
# Modified again by Mr Green to work with ArchBang Linux
#
# Heavy editing/Destruction by Luke Jones. Goal is to do a one click install
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

##
## Set variables, colours, and prompts
##

# Architecture
  ARCHI=$(uname -m)
  UEFI=0
  EFI_DISK="/boot/efi"
  BOOT_DISK="/dev/sda"

# COLORS
    Bold=$(tput bold)
    Underline=$(tput sgr 0 1)
    Reset=$(tput sgr0)

    Green=$(tput setaf 2)

    BRed=${Bold}$(tput setaf 1)
    BBlue=${Bold}$(tput setaf 4)

# Directory variables
  AUI_DIR=$(pwd)
  DESTDIR="/mnt/install"
  mkdir -p $DESTDIR
  SOURCE="$DESTDIR/source"
  mkdir -p $SOURCE
  BYPASS="$DESTDIR/bypass"
  mkdir -p $BYPASS

#
# Set common functions
#
  check_boot_system() {
    if [[ "$(cat /sys/class/dmi/id/sys_vendor)" == 'Apple Inc.' ]] || [[ "$(cat /sys/class/dmi/id/sys_vendor)" == 'Apple Computer, Inc.' ]]; then
      modprobe -r -q efivars || true  # if MAC
    else
      modprobe -q efivarfs            # all others
    fi
    if [[ -d "/sys/firmware/efi/" ]]; then
      ## Mount efivarfs if it is not already mounted
      if [[ -z $(mount | grep /sys/firmware/efi/efivars) ]]; then
        mount -t efivarfs efivarfs /sys/firmware/efi/efivars
      fi
      UEFI=1
      echo "UEFI Mode detected"
    else
      UEFI=0
      echo "BIOS Mode detected"
    fi
  }

  print_line() { 
    printf "%$(tput cols)s\n"|tr ' ' '-'
  }
  print_title() {
    clear
    print_line
    echo -e "# ${BBlue}$1${Reset}"
    print_line
    echo ""
  }

  print_info() {
    #Console width number
    T_COLS=$(tput cols)
    echo -e "${Bold}$1${Reset}\n" | fold -sw $(( $T_COLS - 18 )) | sed 's/^/\t/'
  }

  print_danger() {
    T_COLS=$(tput cols)
    echo -e "${BRed}$1${Reset}\n" | fold -sw $(( $T_COLS - 1 ))
  }

  pause_function() {
    print_line
      read -e -sn 1 -p "Press enter to continue..."
  }

 arch_chroot() {
    arch-chroot $DESTDIR /bin/bash -c "${1}"
  }

check_archbang_requirements() {
  if [[ ${EUID} -ne 0 ]]; then
	print_danger "This script must be run with root privilages (i.e. the 'sudo' command)."
    pause_function
    exit 1
  fi

 if [[ ! -f /usr/bin/pacstrap ]]; then
	print_danger "Please install arch-install-scripts package and try again."
	pause_function
	exit 1
 fi
}

#}}}
#UMOUNT PARTITIONS {{{
umount_partitions(){
  mounted_partitions=(`lsblk | grep ${DESTDIR} | awk '{print $7}' | sort -r`)
  swapoff -a
  for i in ${mounted_partitions[@]}; do
    umount $i
  done
}
#}}}
#CREATE PARTITION SCHEME {{{
create_partitions(){
   # BOOT PART
    if [[ $UEFI -eq 1 ]]; then
        echo "o
        n
        p
        1

        +150M
        w
        "|fdisk ${BOOT_DISK};mkfs.fat /dev/sda1
        echo "n
        p
        2

        +5G
        w
        "|fdisk ${BOOT_DISK};mkfs.ext4 /dev/sda2
        echo "n
        p
        3

        +2G
        w
        "|fdisk ${BOOT_DISK};mkswap /dev/sda3;swapon /dev/sda3
        echo "n
        p
        4


        w
        "|fdisk ${BOOT_DISK};mkfs.ext4 /dev/sda4
        mkdir /mnt/install
        mount /dev/sda2 /mnt/install
        mkdir -p /mnt/install/boot/efi
        mount /dev/sda1 /mnt/install/boot/efi
        mount /dev/sda4 /mnt/install/home
    else
        echo "o
        n
        p
        1

        +5G
        w
        "|fdisk ${BOOT_DISK};mkfs.ext4 /dev/sda1
        echo "n
        p
        2

        +2G
        t
        2
        82
        w
        "|fdisk ${BOOT_DISK};mkswap /dev/sda2;swapon /dev/sda2
        echo "n
        p
        3


        w
        "|fdisk ${BOOT_DISK};mkfs.ext4 /dev/sda3
        mkdir /mnt/install
        mount /dev/sda1 /mnt/install
        mount /dev/sda3 /mnt/install/home
    fi
}

# Install ArchBang from iso image no net required
install_root_image(){
  print_title "INSTALL SYSTEM"
  echo

# mount image file
  AIROOTIMG="/run/archiso/bootmnt/arch/x86_64/airootfs.sfs"
  mkdir -p $BYPASS
  mount $AIROOTIMG $BYPASS
# copy files from bypass to install device
  echo "Installing please wait..."
  rsync -a --info=progress2 $BYPASS/ $DESTDIR/
  umount -l $BYPASS
# set xkeyboard map
  #sed -i "s/us/${KEYMAP_XKB:0:2}/g" $DESTDIR/etc/X11/xorg.conf.d/01-keyboard-layout.conf
# add in password again for sudo
  sed -i '/^%wheel/s/NOPASSWD://g' $DESTDIR/etc/sudoers
# set up kernel for mkiniticpio
  cp /run/archiso/bootmnt/arch/boot/${ARCHI}/vmlinuz ${DESTDIR}/boot/vmlinuz-linux
# copy over new mirrorlist
  cp /etc/pacman.d/mirrorlist ${DESTDIR}/etc/pacman.d/mirrorlist
# Clean up new install
  rm -f ${DESTDIR}/usr/bin/installer &> /dev/null
  rm -f ${DESTDIR}/usr/bin/install-arcade &> /dev/null
  rm -rf ${DESTDIR}/vomi &> /dev/null
  rm -rf ${BYPASS} &> /dev/null
  rm -rf ${DESTDIR}/source &> /dev/null
  rm -rf ${DESTDIR}/src &> /dev/null
  rmdir ${DESTDIR}/bypass &> /dev/null
  rmdir ${DESTDIR}/src &> /dev/null
  rmdir ${DESTDIR}/source &> /dev/null

# clean out archiso files from install
  find ${DESTDIR}/usr/lib/initcpio -name archiso* -type f -exec rm '{}' \;

# systemd
  rm ${DESTDIR}/etc/systemd/system/default.target &> /dev/null
  arch_chroot  "/usr/bin/systemctl -f disable lastmin.service || true"

  sed -i 's/volatile/auto/g' /${DESTDIR}/etc/systemd/journald.conf

# Stop pacman complaining
  arch_chroot "/usr/bin/mkdir -p /var/lib/pacman/sync"
  arch_chroot "/usr/bin/touch /var/lib/pacman/sync/{core.db,extra.db,community.db,multilib.db}"

################################################################################################
# MISC SETUP
  arch_chroot "ln -sf /usr/share/zoneinfo/UTC /etc/localtime"
  arch_chroot "hwclock --systohc --utc";
  echo "KEYMAP=us" > ${DESTDIR}/etc/vconsole.conf

  echo 'LANG="'en_US.UTF-8 UTF-8'"' > ${DESTDIR}/etc/locale.conf
  arch_chroot "sed -i '/'en_US.UTF-8 UTF-8'/s/^#//' /etc/locale.gen"
  arch_chroot "locale-gen"
################################################################################################
# Set up FSTAB
  if [[ ! -f ${DESTDIR}/etc/fstab.aui ]]; then
    cp ${DESTDIR}/etc/fstab ${DESTDIR}/etc/fstab.aui
  else
    cp ${DESTDIR}/etc/fstab.aui ${DESTDIR}/etc/fstab
  fi
  if [[ $UEFI -eq 1 ]]; then
    genfstab -t PARTUUID -p ${DESTDIR} >> ${DESTDIR}/etc/fstab
  else
    genfstab -U -p ${DESTDIR} >> ${DESTDIR}/etc/fstab
  fi
# Do MKINITCPIO
  print_title "CONFIGURE MKINITCPIO"
  arch_chroot "mkinitcpio -p linux"
# Start Installing GRUB2
    print_title "INSTALLING GRUB2"
    if [[ $UEFI -eq 1 ]]; then
        arch_chroot "grub-install --target=x86_64-efi --efi-directory=${EFI_DISK} --bootloader-id=arch_grub --recheck"
    else
        arch_chroot "grub-install --target=i386-pc --recheck ${BOOT_DISK}"
    fi

    arch_chroot "grub-mkconfig -o /boot/grub/grub.cfg"

}
#}}}
###################################################################################

# Destroyed by Luke Jones. Removed original check_connection option as new one implemented.
print_title "AttractMode Arcade Installer v0.3"
check_boot_system
echo
print_info "Checking installer has been run as root, and if there is an active internet connection. Please wait..."
check_archbang_requirements
print_info "${Green}All checks passed"
echo
echo "This script is based on the AIS and AUI scripts written by Helmuth Saatkamp."
echo
#pause_function

umount_partitions
create_partitions
install_root_image

print_title "INSTALLATION COMPLETED!"

umount_partitions

print_info "You can reboot or power off your system."
